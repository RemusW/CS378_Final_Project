Group Members:
- Remus Wong
- Kenji Nakachi
- Grant Shirah
- Sasha Makarovskiy


Team Name: Descent

Game Overview:
Based on the 1995 game Descent, navigate a labyrinth filled with deadly robots in a spacecraft capable of moving with six degrees of freedom (6DOF)

Controls:
- W/S - Forward/Back
- A/D - Horizontal slide left/right
- LShift/Space - Vertical heave down/up
- Q/E - Roll left/right
- Mouse movement - controls yaw pitch
- Left click - Fire laser
- Right click - Fire rocket
- Escape - Main menu

