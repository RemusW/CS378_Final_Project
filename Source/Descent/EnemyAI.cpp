// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"
#include "AProjectile.h"
#include "Components/CapsuleComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/MeshComponent.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"
// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LaserArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("LaserLocation"));
	LaserArrow->SetupAttachment(RootComponent);
	LaserArrow->SetRelativeRotation(FRotator(0, -90.0f, 0));
	LaserArrow->SetRelativeLocation(FVector(0.0f, -230.0f, 0.0f));
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();
	lastShot = 0.0f;
	
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyAI::FireLaser()
{
	// try and fire a projectile
	if (LaserClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
			if(realtimeSeconds-lastShot>1.0){
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = LaserArrow->GetComponentLocation();
				
				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AAProjectile>(LaserClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
				//GEngine->AddOnScreenDebugMessage(-1, 10.0, FColor::Green, "FIRED");
				lastShot = realtimeSeconds;
			}
			
		}
	}
}