// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AProjectile.h"
#include "EnemyAI.generated.h"


UCLASS()
class DESCENT_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyAI();
	float lastShot;
	UPROPERTY(EditAnywhere)
		class UBehaviorTree * BehaviorTree;

	// Left laser projectile spawn location
	UPROPERTY(EditAnywhere)
	class UArrowComponent* LaserArrow;

	UPROPERTY(EditDefaultsOnly, Category=Projectile)
		TSubclassOf<class AAProjectile> LaserClass;

	UFUNCTION(BluePrintCallable)
		void FireLaser();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
