// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DescentGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DESCENT_API ADescentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	ADescentGameModeBase();
};
