// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class DESCENT_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
public:
	AEnemyAIController();
	FORCEINLINE UBlackboardComponent * GetBlackboardComponent() const {	return BlackboardComponent;}

	
protected:
	UPROPERTY()
	class UBehaviorTreeComponent * BehaviorComponent;
	UPROPERTY()
	class UBlackboardComponent * BlackboardComponent;

	virtual void OnPossess(APawn * InPawn) override;
};
