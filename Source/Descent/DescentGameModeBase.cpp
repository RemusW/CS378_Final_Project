// Copyright Epic Games, Inc. All Rights Reserved.


#include "DescentGameModeBase.h"
#include "APlayer.h"

ADescentGameModeBase::ADescentGameModeBase() {
	static ConstructorHelpers::FObjectFinder<UClass> CharacterBPClass(TEXT("Class'/Game/Blueprints/APlayerBP.APlayerBP_C'"));
	if (CharacterBPClass.Object)
	{
		UClass* CharacterBP = (UClass*)CharacterBPClass.Object;
		DefaultPawnClass = CharacterBP;
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, TEXT("Found Player BP!"));
	}
	else
	{
		DefaultPawnClass = AAPlayer::StaticClass();
	}
}