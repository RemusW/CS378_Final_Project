// Fill out your copyright notice in the Description page of Project Settings.


#include "AShip.h"

// Sets default values
AAShip::AAShip()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AAShip::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

