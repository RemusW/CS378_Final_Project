// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "EnemyAI.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
AEnemyAIController::AEnemyAIController(){
    BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
    BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardTreeComponent"));
    //GEngine->AddOnScreenDebugMessage(-1, 10.0, FColor::Blue, BlackboardComponent->GetDebugInfoString(EBlackboardDescription::Full));
    //TODO
}

void AEnemyAIController::OnPossess(APawn * InPawn){
    Super::OnPossess(InPawn);
    AEnemyAI * AICharacter = Cast<AEnemyAI>(InPawn);
    if(BlackboardComponent){
        //GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, "HI");
        if(AICharacter&&AICharacter->BehaviorTree){
            if(AICharacter->BehaviorTree->BlackboardAsset){
                BlackboardComponent->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));
            }
            //PatrolPoints = AICharacter->GetPatrolPoints();
            //BlackboardComponent->SetValueAsObject(NextPointKey, PatrolPoints[CurrentPatrolPoint]);
            //GEngine->AddOnScreenDebugMessage(-1, 10.0, FColor::Green, BlackboardComponent->GetDebugInfoString(EBlackboardDescription::Full));
            if(BehaviorComponent){
                BehaviorComponent->StartTree(*AICharacter->BehaviorTree);
            }
        }
    }
}