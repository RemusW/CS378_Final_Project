// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AProjectile.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "APlayer.generated.h"

UCLASS()
class DESCENT_API AAPlayer : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	UMeshComponent* MeshShip;

	// Left laser mesh
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* L_LaserGun;

	// Left laser projectile spawn location
	UPROPERTY(EditAnywhere)
	class UArrowComponent* L_LaserArrow;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* R_LaserGun;

	UPROPERTY(EditAnywhere)
	class UArrowComponent* R_LaserArrow;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* RocketGun;

	UPROPERTY(EditAnywhere)
	class UArrowComponent* RocketArrow;

	UPROPERTY(EditAnywhere)
	float health;

	UPROPERTY(EditAnywhere)
	float ammo;
	
public:
	// Sets default values for this character's properties
	AAPlayer();

	FORCEINLINE class UCameraComponent* GetCameraComponent()
		const {
		return FirstPersonCameraComponent;
	}

	FORCEINLINE class USpringArmComponent* GetBoomComponent()
		const {
		return CameraBoom;
	}

	UPROPERTY(EditDefaultsOnly, Category=Projectile)
		TSubclassOf<class AAProjectile> LaserClass;

	/** Projectile class to spawn laser */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
		TSubclassOf<class AAProjectile> RocketClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* LaserSound;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* RocketSound;

	UFUNCTION(BlueprintCallable)
		int getHealth();

	UFUNCTION()
		void addToHealth(int value);

	UFUNCTION()
		bool isAlive();

	UFUNCTION(BlueprintCallable)
		int getAmmo();

	UFUNCTION()
		void addToAmmo(int value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USpringArmComponent* CameraBoom;

	// Movement functions
	UFUNCTION()
		void MoveForward(float value);

	UFUNCTION()
		void MoveRight(float value);

	UFUNCTION()
		void MoveUp(float value);

	UFUNCTION()
		void Roll(float value);
	
	UFUNCTION()
		void RotateUp(float value);

	UFUNCTION()
		void RotateRight(float value);

	UFUNCTION()
		void FireLaser();
	
	UFUNCTION()
		void FireRocket();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
};
