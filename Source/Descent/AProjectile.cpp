// Copyright Epic Games, Inc. All Rights Reserve

#include "AProjectile.h"
#include "EnemyAI.h"
#include "APlayer.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"

AAProjectile::AAProjectile() 
{
	// Static reference to the mesh to use for the projectile
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> ProjectileMeshAsset(TEXT("/Game/TwinStick/Meshes/TwinStickProjectile.TwinStickProjectile"));
	// Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	// RootComponent = Root;

	// Use a sphere as a simple collision representation
	// CollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionDetector"));
	// CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	// CollisionComp->OnComponentHit.AddDynamic(this, &AAProjectile::OnHit);		// set up a notification for when this component hits something blocking
	// // Players can't walk on it
	// CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	// CollisionComp->CanCharacterStepUpOn = ECB_No;
	// // CollisionComp->SetupAttachment(ProjectileMesh);
	// RootComponent = CollisionComp;
	
	
	// Create mesh component for the projectile sphere
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh0"));
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->BodyInstance.SetCollisionProfileName("Projectile");
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AAProjectile::OnHit);		// set up a notification for when this component hits something

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement0"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->InitialSpeed = 4000.f;
	ProjectileMovement->MaxSpeed = 6000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0.f; // No gravity

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void AAProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		
		if (Cast<AEnemyAI>(OtherActor))
		{
			AEnemyAI* Enemy = Cast<AEnemyAI>(OtherActor);
			if (EnemyHitSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, EnemyHitSound, Enemy->GetActorLocation());
			}
			Enemy->Destroy();
		}
		if(Cast<AAPlayer>(OtherActor)){
			if (PlayerHitSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, PlayerHitSound, GetActorLocation());
			}
			AAPlayer* Player = Cast<AAPlayer>(OtherActor);
			Player->addToHealth(-20);
			if(!Player->isAlive()){
				UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
				// GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, "You're dead, game over!");
				//Player->Destroy();
			}
		}
	}
	Destroy();
}