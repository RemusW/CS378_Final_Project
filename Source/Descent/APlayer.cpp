// Fill out your copyright notice in the Description page of Project Settings.


#include "APlayer.h"
#include "AProjectile.h"
#include "Components/CapsuleComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AAPlayer::AAPlayer()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	
	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	// Create left laser mesh component
	L_LaserGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("L_LaserGun"));
	L_LaserGun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	L_LaserGun->bCastDynamicShadow = false;
	L_LaserGun->CastShadow = false;
	L_LaserGun->SetRelativeRotation(FRotator(0, 90.0f, 0));
	L_LaserGun->SetupAttachment(RootComponent);

	L_LaserArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("L_LaserLocation"));
	L_LaserArrow->SetupAttachment(L_LaserGun);
	L_LaserArrow->SetRelativeRotation(FRotator(0, -90.0f, 0));
	L_LaserArrow->SetRelativeLocation(FVector(0.0f, -230.0f, 0.0f));

	// Create right laser mesh component
	R_LaserGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("R_LaserGun"));
	R_LaserGun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	R_LaserGun->bCastDynamicShadow = false;
	R_LaserGun->CastShadow = false;
	R_LaserGun->SetRelativeRotation(FRotator(0, 90.0f, 0));
	R_LaserGun->SetupAttachment(RootComponent);

	R_LaserArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("R_LaserLocation"));
	R_LaserArrow->SetupAttachment(R_LaserGun);
	R_LaserArrow->SetRelativeRotation(FRotator(0, -90.0f, 0));
	R_LaserArrow->SetRelativeLocation(FVector(10.0f, 0.0f, 0.0f));
	R_LaserArrow->SetRelativeLocation(FVector(0.0f, -230.0f, 0.0f));
	
	// Create rocket launcher mesh component
	RocketGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rocket Launcher"));
	RocketGun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	RocketGun->bCastDynamicShadow = false;
	RocketGun->CastShadow = false;
	RocketGun->SetRelativeRotation(FRotator(0, 90.0f, 0));
	RocketGun->SetupAttachment(RootComponent);

	RocketArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("RocketLocation"));
	RocketArrow->SetupAttachment(RocketGun);
	RocketArrow->SetRelativeRotation(FRotator(0, -90.0f, 0));
	RocketArrow->SetRelativeLocation(FVector(10.0f, 0.0f, 0.0f));
	RocketArrow->SetRelativeLocation(FVector(0.0f, -230.0f, 0.0f));

	health = 100;
	ammo = 100;
}

int AAPlayer::getHealth()
{
	return health;
}

void AAPlayer::addToHealth(int value)
{
	health += value;
	if (health > 100)
	{
		// max health is 100
		health = 100;
	}
}

bool AAPlayer::isAlive()
{
	if (health <= 0)
		return false;
	return true;
}

int AAPlayer::getAmmo()
{
	return ammo;
}

void AAPlayer::addToAmmo(int value)
{
	ammo += value;
}

// Called when the game starts or when spawned
void AAPlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, FString::Printf(TEXT("Health: %f"), health));
	//GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, FString::Printf(TEXT("Ammo: %f"), ammo));
}

// Called to bind functionality to input
void AAPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveForward", this, &AAPlayer::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AAPlayer::MoveRight);
	InputComponent->BindAxis("MoveUp", this, &AAPlayer::MoveUp);
	InputComponent->BindAxis("Roll", this, &AAPlayer::Roll);
	InputComponent->BindAxis("LookUp", this, &AAPlayer::RotateUp);
	InputComponent->BindAxis("Turn", this, &AAPlayer::RotateRight);

	// PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	// PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	// PlayerInputComponent->BindAxis("Roll", this, &APawn::AddControllerRollInput);

	InputComponent->BindAction("FireLaser", IE_Pressed, this, &AAPlayer::FireLaser);
	InputComponent->BindAction("FireRocket", IE_Pressed, this, &AAPlayer::FireRocket);
}

/* Bound to W and S
 */
void AAPlayer::MoveForward(float value)
{
	AddMovementInput(GetActorForwardVector(), value);

	// TODO: ensure forward and backward movement aligns with up and down rotations
}

/* Bound to D and A
 */
void AAPlayer::MoveRight(float value)
{
	AddMovementInput(GetActorRightVector(), value);
}

/* Bound to LShift and Space
*/
void AAPlayer::MoveUp(float value)
{
	AddMovementInput(GetActorUpVector(), value);
}

void AAPlayer::Roll(float value)
{
	if (value != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		FQuat DeltaRotation = FQuat(GetActorForwardVector(), -1*value*PI/180);
		FQuat ControlQuat = FQuat(PC->GetControlRotation());
		FQuat NewQuat = DeltaRotation * ControlQuat;
		PC->SetControlRotation(NewQuat.Rotator());
	}
}

/* Pitch UP and DOWN, rotates similar to nodding head
 */
void AAPlayer::RotateUp(float value)
{
	if (value != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		FQuat DeltaRotation = FQuat(GetActorRightVector(), value*PI/180);
		FQuat ControlQuat = FQuat(PC->GetControlRotation());
		FQuat NewQuat = DeltaRotation * ControlQuat;
		PC->SetControlRotation(NewQuat.Rotator());
	}
}

/* Yaw RIGHT and LEFT, rotates similar to shaking head side-to-side
 */
void AAPlayer::RotateRight(float value)
{
	if (value != 0.f && Controller && Controller->IsLocalPlayerController())
	{
		APlayerController* const PC = CastChecked<APlayerController>(Controller);
		FQuat DeltaRotation = FQuat(GetActorUpVector(), value*PI/180);
		FQuat ControlQuat = FQuat(PC->GetControlRotation());
		// Rotate the controller by the deltarotation in world space
		FQuat NewQuat = DeltaRotation * ControlQuat;
		PC->SetControlRotation(NewQuat.Rotator());
	}
}

void AAPlayer::FireLaser()
{
	// try and fire a projectile
	if (LaserClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr&& ammo>0)
		{
			const FRotator SpawnRotation = GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocationLeft = ((L_LaserGun != nullptr) ? L_LaserArrow->GetComponentLocation() : GetActorLocation());
			const FVector SpawnLocationRight = ((R_LaserGun != nullptr) ? R_LaserArrow->GetComponentLocation() : GetActorLocation());
			
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<AAProjectile>(LaserClass, SpawnLocationLeft, SpawnRotation, ActorSpawnParams);
			World->SpawnActor<AAProjectile>(LaserClass, SpawnLocationRight, SpawnRotation, ActorSpawnParams);
			ammo -=2;

			// try and play the sound if specified
			if (LaserSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, LaserSound, GetActorLocation());
			}
		}
	}

}

void AAPlayer::FireRocket()
{
	if (RocketClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr && ammo>0)
		{
			const FRotator SpawnRotation = GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((RocketGun != nullptr) ? RocketArrow->GetComponentLocation() : GetActorLocation());
			
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<AAProjectile>(RocketClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			ammo --;

			// try and play the sound if specified
			if (RocketSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, RocketSound, GetActorLocation());
			}
		}
	}

}
