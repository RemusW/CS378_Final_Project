// Fill out your copyright notice in the Description page of Project Settings.

#include "APickupHealth.h"
#include "APlayer.h"
#include "Engine.h"
#include "Components/SphereComponent.h"

// Sets default values
AAPickupHealth::AAPickupHealth()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	collisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	RootComponent = collisionComp;

	health = 25;
	isHidden = false;

	SetReplicates(true);
	SetReplicateMovement(true);
}

void AAPickupHealth::removeFromWorld()
{
	RootComponent->GetChildComponent(0)->SetHiddenInGame(true);
	RootComponent->GetChildComponent(1)->SetHiddenInGame(true);
	isHidden = true;
	GetWorldTimerManager().SetTimer(PickupHealthTimerHandle, this, &AAPickupHealth::addToWorld, 10.0f, false, 10.0f);
}

void AAPickupHealth::addToWorld()
{
	RootComponent->GetChildComponent(0)->SetHiddenInGame(false);
	RootComponent->GetChildComponent(1)->SetHiddenInGame(false);
	isHidden = false;
}

// Called when the game starts or when spawned
void AAPickupHealth::BeginPlay()
{
	Super::BeginPlay();
	
}

bool AAPickupHealth::canPickUp(AAPlayer* player)
{
	if (!isHidden && player != NULL && player->isAlive() && player->getHealth() < 100)
	{
		return true;
	}
	if (!isHidden && player->getHealth() == 100 && GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Player already has maximum health!"));
	return false;
}

void AAPickupHealth::handlePickUp(AAPlayer* player)
{
	if (player != NULL)
	{
		// ** DEBUG ** //
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Magenta, TEXT("Player has been granted additional health!"));

		player->addToHealth(this->health);
	}
}

// Called every frame
void AAPickupHealth::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAPickupHealth::NotifyActorBeginOverlap(AActor* other)
{
	if (other != NULL)
	{
		if (canPickUp(Cast<AAPlayer>(other)))
		{
			handlePickUp(Cast<AAPlayer>(other));
			removeFromWorld();
		}
	}
}

void AAPickupHealth::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}