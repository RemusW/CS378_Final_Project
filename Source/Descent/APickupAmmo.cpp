// Fill out your copyright notice in the Description page of Project Settings.


#include "APickupAmmo.h"
#include "APlayer.h"
#include "Engine.h"
#include "Components/SphereComponent.h"
// Sets default values
AAPickupAmmo::AAPickupAmmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	collisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	RootComponent = collisionComp;

	ammo = 40;
	isHidden = false;

	SetReplicates(true);
	SetReplicateMovement(true);

}

// Called when the game starts or when spawned
void AAPickupAmmo::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAPickupAmmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAPickupAmmo::removeFromWorld()
{
	RootComponent->GetChildComponent(0)->SetHiddenInGame(true);
	isHidden = true;
	GetWorldTimerManager().SetTimer(PickupAmmoTimerHandle, this, &AAPickupAmmo::addToWorld, 10.0f, false, 10.0f);
}

void AAPickupAmmo::addToWorld()
{
	RootComponent->GetChildComponent(0)->SetHiddenInGame(false);
	isHidden = false;
}

bool AAPickupAmmo::canPickUp(AAPlayer* player)
{
	if (!isHidden && player != NULL && player->isAlive())
	{
		return true;
	}
	if (!isHidden && player->getHealth() == 100 && GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Player already has maximum health!"));
	return false;
}

void AAPickupAmmo::handlePickUp(AAPlayer* player)
{
	if (player != NULL)
	{
		// ** DEBUG ** //
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Magenta, TEXT("Player has been granted additional ammo!"));

		player->addToAmmo(this->ammo);
	}
}

void AAPickupAmmo::NotifyActorBeginOverlap(AActor* other)
{
	if (other != NULL)
	{
		if (canPickUp(Cast<AAPlayer>(other)))
		{
			handlePickUp(Cast<AAPlayer>(other));
			removeFromWorld();
		}
	}
}

void AAPickupAmmo::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}