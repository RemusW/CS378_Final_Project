// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "APickupAmmo.generated.h"
class USphereComponent;


UCLASS()
class DESCENT_API AAPickupAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAPickupAmmo();
	UFUNCTION()
		void removeFromWorld();

	UFUNCTION()
		void addToWorld();

	FTimerHandle PickupAmmoTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
		int ammo;

	UPROPERTY()
		bool isHidden;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
		USphereComponent* collisionComp;

private:
	UFUNCTION()
		bool canPickUp(class AAPlayer* player);

	UFUNCTION()
		void handlePickUp(class AAPlayer* player);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(class AActor* other) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
